# Copy to config.py and edit as needed.

game_dir = '/opt/steam/steamapps/common/Stellaris'
workshop_dir = '/opt/steam/steamapps/workshop/content/281990'
mods = {'vanilla': None,
        'alphamod': None,
        'new_horizons': 688086068,
        'nsc': 683230077,
        'isb': 693447055,
        'primitive': 915966120,
        'eutb': 804732593,
        'exoverhaul': ["exo_combat","exo_ai_suite","exo_planets"], #list of mods in load order. Can be recursive (i.e. link to a name that is again a list of mods)
        'exo_combat': "~/Documents/Paradox Interactive/Stellaris/modModding/exoverhaul/combat", # "~" will be converted to User folder
        'exo_ai_suite': "~/Documents/Paradox Interactive/Stellaris/modModding/exoverhaul/ai_suite",
        'exo_planets': "~/Documents/Paradox Interactive/Stellaris/modModding/exoverhaul/planets_enhanced"
        }
